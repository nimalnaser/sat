## Spray autoignition tool (SAT)

A Python code, spray autoignition tool (SAT) to determine the ignition delay time obtained from an ignition quality tester based on the gradient method described in [1-3] is provided. The code is compatible with both versions of Python, 2.x and 3.x.

**References**

[1] Naser, N. (2018). Autoignition behavior of practical fuels. Ph.D. dissertation, King Abdullah University of Science and Technology (KAUST). doi:[10.25781/KAUST-3LN8D](https://doi.org/10.25781/KAUST-3LN8D "10.25781/KAUST-3LN8D").

[2] Naser, N., Yang, S. Y., Kalghatgi, G., & Chung, S. H. (2017). Relating the octane numbers of fuels to ignition delay times measured in an ignition quality tester (IQT). Fuel, 187, 117-127. doi:[10.1016/j.fuel.2016.09.013](https://doi.org/10.1016/j.fuel.2016.09.013 "10.1016/j.fuel.2016.09.013").

[3] Yang, S. Y., Naser, N., Chung, S. H., & Cha, J. (2015). Effect of Temperature, Pressure and Equivalence Ratio on Ignition Delay in Ignition Quality Tester (IQT) Diesel, n-Heptane, and iso-Octane Fuels under Low Temperature Conditions. SAE International Journal of Fuels and Lubricants, 8(3), 537-548. doi:[10.4271/2015-01-9074](https://doi.org/10.4271/2015-01-9074 "10.4271/2015-01-9074").
