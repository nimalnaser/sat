#====================================================================================
# Spray autoignition tool (SAT)
#====================================================================================
#
# Author            : Nimal Naser (nimal.naser@kaust.edu.sa, nimal.naser@gmail.com)
# Institute         : King Abdullah University of Science and Technology (KAUST)
# Department        : Mechanical engineering
# Research center   : Clean combustion research center
# Version           : 1.0
# Created on        : 12 February 2015
# Last modified on  : 12 February 2018
#
# -----------------------------------------------------------------------------------
#
# -----------------------------------------------------------------------------------


# -----------------------------------------------------------------------------------
# Changelog
# -----------------------------------------------------------------------------------


# -----------------------------------------------------------------------------------
# Import module
# -----------------------------------------------------------------------------------
from __future__ import division

# -----------------------------------------------------------------------------------
# Begin timer
# -----------------------------------------------------------------------------------
import timeit
time_start = timeit.default_timer()

# -----------------------------------------------------------------------------------
# Import modules and functions
# -----------------------------------------------------------------------------------
from datetime import datetime
from math import factorial
from matplotlib import ticker as mtick
from matplotlib.backends.backend_pdf import PdfPages
import cantera as ct
import logging
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import re
import scipy as sp
import stat
import statsmodels.api as sm
import sys
import time
import timeit
from scipy.signal import butter, filtfilt
from scipy import optimize, interpolate
from bisect import bisect
from matplotlib.colors import LinearSegmentedColormap
from numpy import nan, inf
import matplotlib.cm as mplcm
from matplotlib.patches import Rectangle

# -----------------------------------------------------------------------------------
# Log file
# -----------------------------------------------------------------------------------
if not os.path.isdir('./Logs'):
    os.makedirs('./Logs')
logging.basicConfig(filename = './Logs/SAT - Log - ' + \
    str(time.strftime("%Y-%m-%d %H:%M:%S")) + '.log', level = logging.DEBUG, \
    filemode = 'w', format = '%(asctime)s\t%(levelname)s\t%(message)s\t', \
    datefmt = "[%Y-%m-%d %H:%M:%S]")

# -----------------------------------------------------------------------------------
# User-defined functions
# -----------------------------------------------------------------------------------
def square_plot(plot):
    x_min, x_max = plot.get_xlim()
    y_min, y_max = plot.get_ylim()
    return plot.set_aspect((x_max - x_min) / (y_max - y_min))
def nearest_power(x, base, mode):
    if mode == '+':
        return int(base ** (math.ceil(math.log(x, base))))
    elif mode == '-':
        return int(base ** (math.floor(math.log(x, base))))
def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError, msg:
        raise ValueError("Window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("Window_size is too small for the polynomials order")
    order_range = range(order + 1)
    half_window = (window_size - 1) // 2
    b = np.mat([[k ** i for i in order_range] for k \
        in range(-half_window, half_window + 1)])
    m = np.linalg.pinv(b).A[deriv] * rate ** deriv * factorial(deriv)
    firstvals = y[0] - np.abs(y[1:half_window + 1][::-1] - y[0])
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve(m[::-1], y, mode='valid')
def butter_lowpass_filter(signal, cut_off_frequency, sampling_rate, order=5):
    nyquist_frequency = 0.5 * sampling_rate
    normal_cut_off_frequency = cut_off_frequency / nyquist_frequency
    b, a = butter(order, normal_cut_off_frequency, btype='low', analog=False)
    y = filtfilt(b, a, signal)
    return y
def cycle_match(variable_cycle, value):
    idx = (np.abs(variable_cycle - value)).argmin()
    return idx
def pdf_metadata(pdfpage):
    pdf_metadata = pdfpage.infodict()
    pdf_metadata['Title'] = 'Spray Autoignition Tool'
    pdf_metadata['Author'] = 'Nimal Naser'
    pdf_metadata['Subject'] = 'Mechanical Engineering'
    pdf_metadata['Keywords'] = ''
    pdf_metadata['CreationDate'] = datetime.today()
    pdf_metadata['ModDate'] = datetime.today()
def init_plot_dark():
    params = {'backend' : 'agg', 'legend.numpoints' : 1,
    'lines.linewidth' : 1.0, 'lines.linestyle' : '-',
    'axes.facecolor' : '#EEEEEE', 'axes.edgecolor' : '#FFFFFF',
    'axes.linewidth' : 0.0, 'axes.grid' : True, 'axes.titlesize' : 'large',
    'axes.labelsize' : 12, 'axes.labelweight' : 'normal',
    'axes.labelcolor' : '000000', 'axes.axisbelow' : True,
    'polaraxes.grid' : True,  'axes3d.grid' : True,
    'axes.color_cycle' : ('#E41A1C', '#377EB8', '#4DAF4A', 
        '#984EA3', '#FF7F00', '#FFFF33', '#A65628', '#F781BF', '#999999'),
    'xtick.major.size' : 4, 'xtick.minor.size' : 2, 'xtick.major.width' : 0,
    'xtick.minor.width' : 0, 'xtick.major.pad' : 6, 'xtick.minor.pad' : 6,
    'xtick.color' : '#555555',  'xtick.labelsize' : 10, 'xtick.direction' : 'in',
    'ytick.major.size' : 4, 'ytick.minor.size' : 2,  'ytick.major.width' : 0, 
    'ytick.minor.width' : 0, 'ytick.major.pad' : 6, 'ytick.minor.pad' : 6, 
    'ytick.color' : '#555555', 'ytick.labelsize' : 10, 'ytick.direction' : 'in',
    'grid.color' : '#FFFFFF', 'grid.linestyle' : '-', 'grid.linewidth' : 0.5,
    'grid.alpha' : 1.0, 'legend.fontsize' : 10, 'legend.borderaxespad' : 0.5,
    'legend.shadow' : False, 'legend.frameon' : True}
    plt.rcParams.update(params)
def init_plot_white():
    params = {'backend' : 'agg', 'legend.numpoints' : 1,
    'lines.linewidth' : 1.0, 'lines.linestyle' : '-',
    'axes.facecolor' : '#FFFFFF', 'axes.edgecolor' : '#000000',
    'axes.linewidth' : 1.0, 'axes.grid' : True, 'axes.titlesize' : 'large',
    'axes.labelsize' : 12, 'axes.labelweight' : 'normal',
    'axes.labelcolor' : '000000', 'axes.axisbelow' : True,
    'polaraxes.grid' : True,  'axes3d.grid' : True,
    'axes.color_cycle' : ('#E41A1C', '#377EB8', '#4DAF4A',
        '#984EA3', '#FF7F00', '#FFFF33', '#A65628', '#F781BF', '#999999'),
    'xtick.major.size' : 4, 'xtick.minor.size' : 2, 'xtick.major.width' : 1,
    'xtick.minor.width' : 1, 'xtick.major.pad' : 6, 'xtick.minor.pad' : 6,
    'xtick.color' : '#000000',  'xtick.labelsize' : 10, 'xtick.direction' : 'in',
    'ytick.major.size' : 4, 'ytick.minor.size' : 2, 'ytick.major.width' : 1,
    'ytick.minor.width' : 1, 'ytick.major.pad' : 6, 'ytick.minor.pad' : 6,
    'ytick.color' : '#000000', 'ytick.labelsize' : 10, 'ytick.direction' : 'in',
    'grid.color' : '#C8C8C8', 'grid.linestyle' : ':', 'grid.linewidth' : 0.5,
    'grid.alpha' : 1.0, 'legend.fontsize' : 10, 'legend.borderaxespad' : 0.5,
    'legend.shadow' : False, 'legend.frameon' : True}
    plt.rcParams.update(params)

# -----------------------------------------------------------------------------------
# Colormap selection
# -----------------------------------------------------------------------------------
colormap = 'gist_heat'
ignore_white = True
marker_edge_color = 'black'

# -----------------------------------------------------------------------------------
# Plot style
# -----------------------------------------------------------------------------------
init_plot_dark()

# -----------------------------------------------------------------------------------
# Obtain data sets and subsets in queue
# -----------------------------------------------------------------------------------
data_folders = []
data_files = []
for root, directories, files in sorted(os.walk('./Processing queue')):
    files = sorted([f for f in files if os.path.splitext(f)[1] in ('.pre',)])
    for file in files:
        data_folders.append(root)
        data_files.append((re.findall(r"[-+]?\d*\.\d+|\d+", file)[-2].zfill(5), \
            re.findall(r"[-+]?\d*\.\d+|\d+", file)[-1].zfill(3), \
            os.path.join(root, file)))
data_folders = np.unique(data_folders)
data_files = sorted(data_files)
data_files = np.array(data_files)
logging.info(u'\u2714\t\t\t\u25CF Folders in queue: \n\t\t\t\t\t\t\t\t\t\t\t\t' + \
    u'\u229D %s', u'\n\t\t\t\t\t\t\t\t\t\t\t\t\u229D ' \
    .join(os.path.basename(file) for file in data_folders))
logging.info(u'\u2714\t\t\t\u25CF Files in queue: \n\t\t\t\t\t\t\t\t\t\t\t\t' + \
    u'\u229D %s', u'\n\t\t\t\t\t\t\t\t\t\t\t\t\u229D ' \
    .join(os.path.basename(file) for file in [item[2] for item in data_files]))

# -----------------------------------------------------------------------------------
# Create processed data folder
# -----------------------------------------------------------------------------------
if not os.path.isdir('./Processed data'):
    logging.warning(u'\u2716\t\t\t\u25C6 Processed data folder does not exist')
    os.makedirs('./Processed data')
    logging.info(u'\u2714\t\t\t\u25C6 Processed data folder created')

# -----------------------------------------------------------------------------------
# Loop initialization
# -----------------------------------------------------------------------------------
local_counter = 1
global_counter = 1
initial_loop = 1
low_temperature_heat_relase = 0
result_all = open('./Processed data/SAT - Results.dat','w')
result_all.write('#============================================================' + \
    '================================================\n')
result_all.write('# Spray Autoignition Tool (SAT)\n')
result_all.write('#============================================================' + \
    '================================================\n')
result_all.write('#\n')
result_all.write('# Author                  : Nimal Naser ' + \
    '(nimal.naser@gmail.com, nimal.naser@kaust.edu.sa)\n')
result_all.write('# Institute               : King Abdullah University of ' + \
    'Science and Technology (KAUST)\n')
result_all.write('# Department              : Mechanical Engineering\n')
result_all.write('# Research center         : Clean Combustion Research ' + \
    'Center (CCRC)\n')
result_all.write('# Instrument/Device       : KAUST Research - Ignition ' + \
    'Quality Tester (KR-IQT)\n')
result_all.write('# Data processing date    : ' + \
    str(datetime.today().strftime("%Y-%m-%d %H:%M:%S")) + '\n')
result_all.write('#\n')
result_all.write('# ------------------------------------------------------' + \
    '-----------------------------\n')
result_all.write('# Results\n')
result_all.write('# ------------------------------------------------------' + \
    '-----------------------------\n')
result_all.writelines('Serial number (#)\tDevice\tRun (#)\t' + \
    'Charge test pressure (bar)\tTotal ignition delay (ms)\t' + \
    'Low temperature ignition delay (ms)\tPercentage low temperature ignition ' + \
    'delay (%)\tLow temperature heat release duration (ms)\tPercentage low ' + \
    'temperature heat release duration (%)\tIgnition pressure (bar)\t' + \
    'Percentage ignition pressure (%)\tLow temperature ignition pressure (bar)\t' + \
    'Percentage low temperature ignition pressure (%)\tMaximum charge pressure ' + \
    '(bar)\tPercentage maximum charge pressure (%)\t' + \
    'Maximum charge pressure delay (ms)\tPercentage maximum charge pressure \t' + \
    'delay (%)Minimum charge pressure (bar)\tPercentage minimum charge ' + \
    'pressure (%)\tMinimum charge pressure delay (ms)\tPercentage minimum ' + \
    'charge pressure delay (%)\tPressure recovery delay (ms)\tPercentage ' + \
    'pressure recovery delay (%)\tPressure recovery gradient (bar/ms)\t' + \
    'Pressure rise maximum gradient delay (ms)\tPercentage pressure ' + \
    'rise maximum gradient delay (%)\tMaximum gradient during ' + \
    'ignition (bar/ms)\tBurning rate (bar/ms)\tBurning duration (ms)\t ' + \
    'Ignition determination\n')

# -----------------------------------------------------------------------------------
# Loop through data sets and subsets in queue
# -----------------------------------------------------------------------------------
for data_file in data_files:
    logging.info(u'\u2714\t\t\t\u25FC Data set %s', data_file[0])
    if not os.path.isdir('./Processed data/SAT - Result - ' + data_file[0] + \
        '/Power spectral density')  \
        or not os.path.isdir('./Processed data/SAT - Result - ' + data_file[0] + \
        '/Processed pressure traces') \
        or not os.path.isdir('./Processed data/SAT - Result - ' + data_file[0] + \
        '/Representative injection'):
        logging.warning(u'\u2716\t\t\t\u25C6 Processed data folder does not exist')
        os.makedirs('./Processed data/SAT - Result - ' + data_file[0] + \
        '/Power spectral density')
        os.makedirs('./Processed data/SAT - Result - ' + data_file[0] + \
        '/Processed pressure traces')
        os.makedirs('./Processed data/SAT - Result - ' + data_file[0] + \
        '/Representative injection')
        logging.info(u'\u2714\t\t\t\t\u25C6 Processed data folder created')
        if not os.path.isdir('./Processed data/SAT - Result - ' + data_file[0]):
            logging.warning(u'\u2716\t\t\t\u25C6 Processed data folder does ' + \
                'not exist')
            os.makedirs('./Processed data/SAT - Result - ' + data_file[0])
            logging.info(u'\u2714\t\t\t\t\u25C6 Processed data folder created')
    else:
        logging.info(u'\u2714\t\t\t\t\u25C6 Processed data folder exists')
    logging.info(u'\u2714\t\t\t\t\u25C6 Data subset %s', data_file[1])


# -----------------------------------------------------------------------------------
# Create result file
# -----------------------------------------------------------------------------------
    if initial_loop == 1:
        result = open('./Processed data/SAT - Result - ' + data_file[0] + \
            '/SAT - Result - ' + data_file[0] + '.dat','w')
        result.write('#========================================================' + \
            '====================================================\n')
        result.write('# Spray Autoignition Tool (SAT)\n')
        result.write('#========================================================' + \
            '====================================================\n')
        result.write('#\n')
        result.write('# Author                  : Nimal Naser ' + \
            '(nimal.naser@gmail.com, nimal.naser@kaust.edu.sa)\n')
        result.write('# Institute               : King Abdullah University of ' + \
            'Science and Technology (KAUST)\n')
        result.write('# Department              : Mechanical Engineering\n')
        result.write('# Research center         : Clean Combustion Research ' + \
            'Center (CCRC)\n')
        result.write('# Instrument/Device       : KAUST Research - Ignition ' + \
            'Quality Tester (KR-IQT)\n')
        result.write('# Data processing date    : ' + \
            str(datetime.today().strftime("%Y-%m-%d %H:%M:%S")) + '\n')
        result.write('# Chamber volume          : 0.00021 m^3\n')
        result.write('# Injection pressure      : 225 bar\n')
        result.write('# Ignition determination  : Gradient method\n')
        result.write('#\n')
        result.write('# -----------------------------------------------------' + \
            '------------------------------\n')
        result.write('# Result of data set ' + str(data_file[0]) + '\n')
        result.write('# -----------------------------------------------------' + \
            '------------------------------\n')
        result.writelines('Serial number (#)\tInjection number (#)\tCharge test ' + \
            'pressure (bar)\tLow temperature ignition\tTotal ignition ' + \
            'delay (ms)\tLow temperature ignition delay (ms)\tPercentage low ' + \
            'temperature ignition delay (%)\tLow temperature heat release ' + \
            'duration (ms)\tPercentage low temperature heat release ' + \
            'duration (%)\tIgnition pressure (bar)\tPercentage ignition ' + \
            'pressure (%)\tLow temperature ignition pressure (bar)\t' + \
            'Percentage low temperature ignition pressure (%)\tMaximum ' + \
            'charge pressure (bar)\tPercentage maximum charge ' + \
            'pressure (%)\tMaximum charge pressure delay (ms)\t' + \
            'Percentage maximum charge pressure delay (%)\t' + \
            'Minimum charge pressure (bar)\tPercentage minimum ' + \
            'charge pressure (%)\tMinimum charge pressure delay (ms)\t' + \
            'Percentage minimum charge pressure delay (%)\t' + \
            'Pressure recovery delay (ms)\tPercentage pressure ' + \
            'recovery delay (%)\tPressure recovery gradient (bar/ms)\t' + \
            'Pressure rise maximum gradient delay (ms)\tPercentage pressure ' + \
            'rise maximum gradient delay (%)\tMaximum gradient during ' + \
            'ignition (bar/ms)\tBurning rate (bar/ms)\tBurning duration (ms)\n')
        initial_loop = 0


# -----------------------------------------------------------------------------------
# Create processed pressure file
# -----------------------------------------------------------------------------------
    result_pressure = open('./Processed data/SAT - Result - ' + data_file[0] + \
        '/Processed pressure traces/SAT - Result - Processed pressure - ' + data_file[0] + ' - ' + data_file[1] +'.dat','w')
    result_pressure.write('#============================================================================================================\n')
    result_pressure.write('# Spray Autoignition Tool (SAT)\n')
    result_pressure.write('#============================================================================================================\n')
    result_pressure.write('#\n')
    result_pressure.write('# Author                  : Nimal Naser (nimal.naser@gmail.com, nimal.naser@kaust.edu.sa)\n')
    result_pressure.write('# Institute               : King Abdullah University of Science and Technology (KAUST)\n')
    result_pressure.write('# Department              : Mechanical Engineering\n')
    result_pressure.write('# Research center         : Clean Combustion Research Center (CCRC)\n')
    result_pressure.write('# Instrument/Device       : KAUST Research - Ignition Quality Tester (KR-IQT)\n')
    result_pressure.write('# Data processing date    : ' + str(datetime.today().strftime("%Y-%m-%d %H:%M:%S")) + '\n')
    result_pressure.write('# Chamber volume          : 0.00021 m^3\n')
    result_pressure.write('# Injection pressure      : 225 bar\n')
    result_pressure.write('# Ignition determination  : Gradient method\n')
    result_pressure.write('#\n')
    result_pressure.write('# -----------------------------------------------------------------------------------\n')
    result_pressure.write('# Processed pressure of data set ' + str(data_file[0]) + ' - ' + str(data_file[1]) +'\n')
    result_pressure.write('# -----------------------------------------------------------------------------------\n')
    result_pressure.write('# 1. Time (ms) | 2. Raw pressure (bar) | 3. Smoothened pressure (bar) 4. | Needle lift (ms)\n')
    result_pressure.writelines('Time\tRaw pressure\tSmoothened pressure\tNeedle lift\n')
    # result_pressure.writelines('(\tRaw pressure\tSmoothened pressure\tNeedle lift\n')


# -----------------------------------------------------------------------------------
# Import data subset
# -----------------------------------------------------------------------------------
    with open(data_file[2], "r") as data:                                                               # Open data subset
        logging.info(u'\u2714\t\t\t\t\t\u27A4 Opened data subset')
        while True:
            line = data.readline()
            if not line.startswith('#'):
                break
        data_header = [i for i in line.strip().split(',') if i]
        logging.info(u'\u2714\t\t\t\t\t\u27A4 Obtained data header')
        _data_ = np.genfromtxt(data, names = data_header, skiprows = 1, dtype = None, delimiter = ',')
    _data_.dtype.names = [j.replace('_', ' ') for j in _data_.dtype.names]
    logging.info(u'\u2714\t\t\t\t\t\u27A4 Generated data array')
    time = _data_['Time']
    pressure = _data_['Combustion Chamber Pressure'] * 0.06894759086775369
    needle_lift = _data_['Injector Needle Lift'] * 25.4
    logging.info(u'\u2714\t\t\t\t\t\u27A4 Generated time, combustion chamber pressure, needle lift array')


# -----------------------------------------------------------------------------------
# Calculate sampling interval and determine indices where sampling interval changes
# -----------------------------------------------------------------------------------
    sampling_interval = np.around(np.diff(time), 2)                             # Determine sampling interval
    sampling_interval_min = round(np.amin(sampling_interval), 2)                # Determine the minimum sampling interval
    dt = sampling_interval_min / 1000
    sampling_rate = 1 / dt          # Sampling rate (in Hz)
    logging.info(u'\u2714\t\t\t\t\t\u27A4 Calculated sampling interval and sampling rate')


# -----------------------------------------------------------------------------------
# Filtering
# -----------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------
# Filter design
# -----------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------
# Power spectral density calculations before applying filter
# -----------------------------------------------------------------------------------
    power_real, frequency = plt.psd(pressure * 100000, NFFT = int(nearest_power(len(pressure), 2, '+')), Fs = 1 / dt, scale_by_freq = False)


# -----------------------------------------------------------------------------------
# Convert power spectral density to decibel
# -----------------------------------------------------------------------------------
    power = 10 * np.log10(power_real)


# -----------------------------------------------------------------------------------
# Apply filter to power spectral density
# -----------------------------------------------------------------------------------
    power_filtered = sm.nonparametric.lowess(power, frequency, frac=0.004)[:,1]
    d_power_filtered_d_frequency = savitzky_golay(power_filtered, window_size = 101, order = 9, deriv = 1)


# -----------------------------------------------------------------------------------
# Calculate cut-off frequencies
# -----------------------------------------------------------------------------------
    for i in range (10, len(power)):
        if 0 <= np.round(d_power_filtered_d_frequency[i], 3):
            cut_off_frequency = frequency[i]
            break
    logging.info(u'\u2714\t\t\t\t\t\u27A4 Obtained cut-off frequency for combustion chamber pressure curve')
    cut_off_frequency = 1500


# -----------------------------------------------------------------------------------
# Plot power spectral density of cylinder pressure (combustion)
# -----------------------------------------------------------------------------------
    with PdfPages('./Processed data/SAT - Result - ' + data_file[0] + '/Power spectral density/SAT - Result - ' + data_file[0] + \
        ' - ' + data_file[1] + ' - Power spectral density of combustion chamber pressure.pdf') as power_spectral_density_pressure:
        _power_spectral_density_pressure_vs_frequency_ = plt.figure(figsize=(5, 5))
        _power_spectral_density_pressure_vs_frequency_.clf()
        power_spectral_density_pressure_vs_frequency = plt.subplot(211)
        power_spectral_density_pressure_vs_frequency.plot(frequency, power, label = r'\textit{Raw}')
        power_spectral_density_pressure_vs_frequency.plot(frequency, power_filtered, label = r'\textit{Filtered}')
        power_spectral_density_pressure_vs_frequency.axvline(x=cut_off_frequency, linewidth=0.5, linestyle=':', color='r')
        plt.xscale('log')
        power_spectral_density_pressure_vs_frequency.grid(True, which="major", lw=0.5)
        power_spectral_density_pressure_vs_frequency.grid(True, which="minor", lw=0.1)
        power_spectral_density_pressure_vs_frequency.set_xlabel(r'\textit{Frequency (Hz)}', labelpad = 6)
        power_spectral_density_pressure_vs_frequency.set_ylabel(r'\textit{{Power spectral density (dB)}}', labelpad = 6)
        power_spectral_density_pressure_vs_frequency.legend(loc = 'center left', bbox_to_anchor = (1.025, 0.85))
        power_spectral_density_pressure_vs_frequency = plt.subplot(212)
        power_spectral_density_pressure_vs_frequency.plot(frequency, d_power_filtered_d_frequency, label = r'\textit{}')
        # power_spectral_density_pressure_vs_frequency.plot(frequency, power_filtered, label = r'\textit{Filtered}')
        power_spectral_density_pressure_vs_frequency.axvline(x=cut_off_frequency, linewidth=0.5, linestyle=':', color='r')
        plt.xscale('log')
        power_spectral_density_pressure_vs_frequency.grid(True, which="major", lw=0.5)
        power_spectral_density_pressure_vs_frequency.grid(True, which="minor", lw=0.1)
        power_spectral_density_pressure_vs_frequency.set_xlabel(r'\textit{Frequency (Hz)}', labelpad = 6)
        power_spectral_density_pressure_vs_frequency.set_ylabel(r'\textit{{$\frac{d(Power spectral density)}{d\theta}$}}', labelpad = 6)
        power_spectral_density_pressure_vs_frequency.legend(loc = 'center left', bbox_to_anchor = (1.025, 0.85))
        plt.tight_layout()
        power_spectral_density_pressure.savefig(bbox_inches='tight')
        plt.close()
        pdf_metadata(power_spectral_density_pressure)
    logging.info(u'\u2714\t\t\t\t\t\u27A4 Generated figure of power spectral density of combustion chamber pressure')   


# -----------------------------------------------------------------------------------
# Apply low pass butterworth filter on cylinder pressure
# -----------------------------------------------------------------------------------
    pressure_smooth = sm.nonparametric.lowess(pressure, time, is_sorted=True, frac=0.04, it=0)[:,1]


# -----------------------------------------------------------------------------------
# Write processed pressure to result file
# -----------------------------------------------------------------------------------
    np.savetxt(result_pressure, np.column_stack((time, pressure, pressure_smooth, needle_lift)), fmt='%.2f', delimiter='\t')


# -----------------------------------------------------------------------------------
# Determine start of injection
# -----------------------------------------------------------------------------------
    i_needle_lift_maximum = np.min(np.argwhere(needle_lift == np.max(needle_lift)))
    time_start_injection = time[i_needle_lift_maximum]


# -----------------------------------------------------------------------------------
# Initial chamber pressure
# -----------------------------------------------------------------------------------
    pressure_charge_initial = np.mean(pressure[0: i_needle_lift_maximum])


# -----------------------------------------------------------------------------------
# Determine pressure recovery point
# -----------------------------------------------------------------------------------
    i_pressure_recovery = i_needle_lift_maximum + bisect(pressure_smooth[i_needle_lift_maximum:-1], pressure_charge_initial)
    time_pressure_recovery = time[i_pressure_recovery]
    pressure_recovery = pressure_smooth[i_pressure_recovery]


# -----------------------------------------------------------------------------------
# Maximum chamber pressure
# -----------------------------------------------------------------------------------
    for i in range(i_pressure_recovery, len(pressure_smooth)):
        slope = (pressure_smooth[i + 1] - pressure_smooth[i]) / (time[i + 1] - time[i])
        if slope <= 0:
            i_pressure_approximate_maximum = i
            break
    i_pressure_maximum = np.min(np.argwhere(pressure_smooth == np.max(pressure_smooth[0:i_pressure_approximate_maximum])))
    pressure_maximum = pressure_smooth[i_pressure_maximum]
    time_pressure_maximum = time[i_pressure_maximum]


# -----------------------------------------------------------------------------------
# Minumum chamber pressure
# -----------------------------------------------------------------------------------
    i_pressure_minimum = np.min(np.argwhere(pressure_smooth == np.min(pressure_smooth[i_needle_lift_maximum:i_pressure_maximum])))
    pressure_minimum = pressure_smooth[i_pressure_minimum]
    time_pressure_minimum = time[i_pressure_minimum]


# -----------------------------------------------------------------------------------
# Slope at pressure recovery point
# -----------------------------------------------------------------------------------
    gradient_pressure_recovery = (pressure_smooth[i_pressure_recovery + 1] - pressure_smooth[i_pressure_recovery]) \
        / (time[i_pressure_recovery + 1] - time[i_pressure_recovery])

# -----------------------------------------------------------------------------------
# Determine gradient of pressure signal
# -----------------------------------------------------------------------------------
    gradient_pressure = np.zeros(shape = (len(pressure_smooth), ))
    for i in range(len(gradient_pressure) - 1):
        gradient_pressure[i] = (pressure_smooth[i + 1] - pressure_smooth[i]) \
            / (time[i + 1] - time[i])
    gradient_pressure_smooth = savitzky_golay(gradient_pressure, 11, 1)
    dslope_dt = np.gradient(gradient_pressure_smooth)
    dslope_dt_smooth = savitzky_golay(dslope_dt, 11, 1)
    i_gradient_pressure_rise_maximum = np.min(np.argwhere(gradient_pressure_smooth == np.max(gradient_pressure_smooth[i_pressure_recovery:i_pressure_maximum])))
    gradient_pressure_rise_maximum = gradient_pressure[i_gradient_pressure_rise_maximum]
    pressure_gradient_pressure_rise_maximum = pressure_smooth[i_gradient_pressure_rise_maximum]
    time_pressure_rise_maximum_gradient = time[i_gradient_pressure_rise_maximum]


# -----------------------------------------------------------------------------------
# Determine start of ignition
# -----------------------------------------------------------------------------------
    gradients_intersection = np.linalg.solve(np.array([[gradient_pressure_rise_maximum, -1], [gradient_pressure_recovery, -1]]), \
        np.array([gradient_pressure_rise_maximum * time_pressure_rise_maximum_gradient - pressure_gradient_pressure_rise_maximum, \
        gradient_pressure_recovery * time_pressure_recovery - pressure_recovery]))
    i_ignition = i_pressure_recovery + bisect(time[i_pressure_recovery:i_pressure_maximum], gradients_intersection[0])
    time_start_ignition = time[i_ignition]
    pressure_ignition = pressure_smooth[i_ignition]


# -----------------------------------------------------------------------------------
# Slope line at pressure recovery point
# -----------------------------------------------------------------------------------
    x_pressure_recovery = np.linspace(time_pressure_recovery, time[-1], 100)
    y_pressure_recovery = pressure_recovery + gradient_pressure_recovery * (x_pressure_recovery - time_pressure_recovery)


# -----------------------------------------------------------------------------------
# Slope line at ignition
# -----------------------------------------------------------------------------------
    y_pressure_ignition = np.linspace(0, pressure_gradient_pressure_rise_maximum, 100)
    x_pressure_ignition = time_pressure_rise_maximum_gradient + (y_pressure_ignition - pressure_gradient_pressure_rise_maximum) / gradient_pressure_rise_maximum


# -----------------------------------------------------------------------------------
# Burn rate and duration
# -----------------------------------------------------------------------------------
    for i in range(i_ignition, i_pressure_maximum):
        if pressure_smooth[i] >= 0.8 * pressure_ignition + 0.2 * pressure_maximum:
            i_burn_start = i
            break
    for i in range(i_ignition, i_pressure_maximum):
        if pressure_smooth[i] >= 0.8 * pressure_maximum + 0.2 * pressure_ignition:
            i_burn_end = i
            break
    burning_rate = (pressure_smooth[i_burn_end] - pressure_smooth[i_burn_start]) / (time[i_burn_end] - time[i_burn_start])
    burning_duration = time[i_burn_end] - time[i_burn_start]


# -----------------------------------------------------------------------------------
# Low temperature heat release
# -----------------------------------------------------------------------------------
    i_lthr_inflection = []
    for i in (range(i_pressure_recovery, i_ignition)):
        if dslope_dt_smooth[i] > 0 and dslope_dt_smooth[i + 1] < 0:
            i_lthr_inflection = np.append(i_lthr_inflection, i)
    # i_lthr_inflection = np.int64(i_lthr_inflection)
    if i_lthr_inflection.size:
        i_slope_low_temperature_heat_release_minimum = np.min(np.argwhere(gradient_pressure_smooth == np.min(gradient_pressure_smooth[i_lthr_inflection[0]:i_ignition])))
        i_slope_low_temperature_heat_release_maximum = np.min(np.argwhere(gradient_pressure_smooth == np.max(gradient_pressure_smooth[i_pressure_recovery:i_lthr_inflection[0]])))
        gradient_lthr_minimum = (pressure_smooth[i_slope_low_temperature_heat_release_minimum + 1] - \
            pressure_smooth[i_slope_low_temperature_heat_release_minimum]) / \
            (time[i_slope_low_temperature_heat_release_minimum + 1] - time[i_slope_low_temperature_heat_release_minimum])
        gradient_lthr_maximum = (pressure_smooth[i_slope_low_temperature_heat_release_maximum + 1] - \
            pressure_smooth[i_slope_low_temperature_heat_release_maximum]) / \
            (time[i_slope_low_temperature_heat_release_maximum + 1] - time[i_slope_low_temperature_heat_release_maximum])
        time_lthr_minimum = time[i_slope_low_temperature_heat_release_minimum]
        time_lthr_maximum = time[i_slope_low_temperature_heat_release_maximum]
        pressure_lthr_minimum = pressure_smooth[i_slope_low_temperature_heat_release_minimum]
        pressure_lthr_maximum = pressure_smooth[i_slope_low_temperature_heat_release_maximum]


# -----------------------------------------------------------------------------------
# Determine start of ignition
# -----------------------------------------------------------------------------------
        lthr_gradients_intersection = np.linalg.solve(np.array([[gradient_lthr_maximum, -1], [gradient_lthr_minimum, -1]]), \
            np.array([gradient_lthr_maximum * time_lthr_maximum - pressure_lthr_maximum, \
            gradient_lthr_minimum * time_lthr_minimum - pressure_lthr_minimum]))
        # print time_lthr_ignition_gradient_method
        i_lthr_ignition_gradient_method = i_pressure_recovery + bisect(time[i_pressure_recovery:i_pressure_maximum], lthr_gradients_intersection[0])
        time_lthr_ignition_gradient_method = time[i_lthr_ignition_gradient_method]
        pressure_lthr_ignition_gradient_method = pressure_smooth[i_lthr_ignition_gradient_method]
        # print pressure_recovery, pressure_lthr_ignition_gradient_method, pressure_ignition
        if pressure_recovery <= pressure_lthr_ignition_gradient_method <= pressure_ignition:
            low_temperature_heat_relase = 1
        else:
            low_temperature_heat_relase = 0


# ------------------------------------------------------------------------------------------------------------
# Slope line at pressure recovery point
# ------------------------------------------------------------------------------------------------------------
    if low_temperature_heat_relase == 1:
        i_lthr_ignition_iqt_method = i_pressure_recovery + bisect(pressure_smooth[i_pressure_recovery:-1], pressure_charge_initial + 1.38)
        time_lthr_ignition_iqt_method = time[i_lthr_ignition_iqt_method]
        pressure_lthr_ignition_iqt_method = pressure[i_lthr_ignition_iqt_method]
        if time_lthr_ignition_gradient_method < time_lthr_ignition_iqt_method:
            method_start_lthr_ignition = 'Gradient method'
            time_start_ignition_lthr = time_lthr_ignition_gradient_method
            pressure_lthr_ignition = pressure_lthr_ignition_gradient_method
            x_low_temperature_heat_release_minimum = np.linspace(time_lthr_ignition_gradient_method, time_lthr_minimum, 100)
            y_low_temperature_heat_release_minimum = pressure_lthr_minimum + \
                gradient_lthr_minimum * (x_low_temperature_heat_release_minimum - time_lthr_minimum)


# -----------------------------------------------------------------------------------
# Slope line at ignition
# -----------------------------------------------------------------------------------
            x_low_temperature_heat_release_maximum = np.linspace(time_lthr_maximum, time_lthr_ignition_gradient_method, 100)
            y_low_temperature_heat_release_maximum = pressure_lthr_maximum + \
                gradient_lthr_maximum * (x_low_temperature_heat_release_maximum - time_lthr_maximum)
        else:
            method_start_lthr_ignition = 'IQT method'       
            time_start_ignition_lthr = time_lthr_ignition_iqt_method
            pressure_lthr_ignition = pressure_charge_initial + 1.38



# -----------------------------------------------------------------------------------
# Ignition delay calculations
# -----------------------------------------------------------------------------------
    total_ignition_delay = time_start_ignition - time_start_injection


# -----------------------------------------------------------------------------------
# Other calculations
# -----------------------------------------------------------------------------------
    delay_pressure_recovery = time_pressure_recovery - time_start_injection
    delay_pressure_minimum = time_pressure_minimum - time_start_injection
    delay_pressure_rise_maximum_gradient = time_pressure_rise_maximum_gradient - time_start_injection
    delay_pressure_maximum = time_pressure_maximum - time_start_injection


# -----------------------------------------------------------------------------------
# Percentage calculations
# -----------------------------------------------------------------------------------
    percentage_pressure_ignition = ((pressure_ignition - pressure_charge_initial) / pressure_charge_initial) * 100
    percentage_pressure_maximum = ((pressure_maximum - pressure_charge_initial) / pressure_charge_initial) * 100
    percentage_pressure_minimum = ((pressure_minimum - pressure_charge_initial) / pressure_charge_initial) * 100
    percentage_pressure_gradient_pressure_rise_maximum = ((pressure_gradient_pressure_rise_maximum - pressure_charge_initial) / pressure_charge_initial) * 100
    percentage_delay_pressure_recovery = (delay_pressure_recovery / total_ignition_delay) * 100
    percentage_delay_pressure_minimum = (delay_pressure_minimum / total_ignition_delay) * 100
    percentage_delay_pressure_maximum = (delay_pressure_maximum / total_ignition_delay) * 100
    percentage_delay_gradient_pressure_rise_maximum = (delay_pressure_rise_maximum_gradient / total_ignition_delay) * 100
    

# -----------------------------------------------------------------------------------
# Low temperature calculations
# -----------------------------------------------------------------------------------
    if low_temperature_heat_relase == 1:
        total_ignition_delay_lthr = time_start_ignition_lthr - time_start_injection
        duration_lthr = total_ignition_delay - total_ignition_delay_lthr
        percentage_low_temperature_ignition_delay = (total_ignition_delay_lthr / total_ignition_delay) * 100
        percentage_duration_lthr = (duration_lthr / total_ignition_delay) * 100
        percentage_pressure_lthr_ignition = ((pressure_lthr_ignition - pressure_charge_initial) / pressure_charge_initial) * 100


# -----------------------------------------------------------------------------------
# Plot chamber pressure
# -----------------------------------------------------------------------------------
    with PdfPages('./Processed data/SAT - Result - ' + data_file[0] + '/SAT - Result - ' + data_file[0] + \
        ' - ' + data_file[1] + ' - Pressure trace.pdf') as pressure_time:
        _pressure_vs_time_ = plt.figure(figsize=(5, 5))
        _pressure_vs_time_.clf()
        needle_vs_time = plt.subplot(111)
        label_1, = needle_vs_time.plot(time, needle_lift, color='#4DAF4A', label = r'\textit{Needle lift}', zorder = 1)
        needle_vs_time.set_xlabel(r'\textit{Time (ms)}', labelpad = 6)
        needle_vs_time.set_ylabel(r'\textit{Needle lift (mm)}', labelpad = 6)
        pressure_vs_time = needle_vs_time.twinx()
        label_2, = pressure_vs_time.plot(time, pressure, label = r'\textit{Raw}', zorder = 3)
        label_3, = pressure_vs_time.plot(time, pressure_smooth, label = r'\textit{Filtered}', zorder = 4)           
        pressure_vs_time.set_ylabel(r'\textit{Combustion chamber pressure (MPa)}', labelpad = 6)
        pressure_vs_time.axvline(time_start_injection, color = '#484848', linestyle = ':', linewidth = 0.75, zorder = 6)
        pressure_vs_time.axvline(time_start_ignition, color = '#484848', linestyle = ':', linewidth = 0.75, zorder = 6)
        pressure_vs_time.axhline(pressure_charge_initial, color = '#484848', linestyle = ':', linewidth = 0.75, zorder = 5)
        pressure_vs_time.plot(x_pressure_recovery, y_pressure_recovery, linewidth = 0.75, color = '#484848', zorder = 2)
        pressure_vs_time.plot(x_pressure_ignition, y_pressure_ignition, linewidth = 0.75, color = '#484848', zorder = 2)
        pressure_vs_time.grid(False)
        pressure_vs_time.set_ylim(bottom = 0)
        primary_ticks = len(pressure_vs_time.yaxis.get_major_ticks())
        needle_vs_time.yaxis.set_major_locator(mtick.LinearLocator(primary_ticks))
        needle_vs_time.yaxis.tick_right()
        needle_vs_time.yaxis.set_label_position("right")
        pressure_vs_time.yaxis.tick_left()
        pressure_vs_time.yaxis.set_label_position("left")           
        y_min, y_max = pressure_vs_time.get_ylim()
        x_min, x_max = pressure_vs_time.get_xlim()
        pressure_vs_time.annotate(r'\textit{$P_{initial}$ = ' + str(np.round(pressure_charge_initial, 2)) + ' bar}', \
            (0.77, (pressure_charge_initial - y_min) / (y_max - y_min) + 0.0075), color = '#000000', textcoords = 'axes fraction', \
            fontsize = 8, zorder = 7)
        pressure_vs_time.annotate('', xy = ((time_start_injection - x_min) / (x_max - x_min), 0.33), \
            xycoords = 'axes fraction', xytext = ((time_start_ignition - x_min) / (x_max - x_min), 0.33), \
            textcoords = 'axes fraction', fontsize = 7, color = '#484848', zorder = 7, arrowprops = dict(edgecolor='#484848', \
            facecolor='#484848', arrowstyle = '<|-|>', linewidth = 0.75, shrinkA = 0, shrinkB = 0))
        pressure_vs_time.annotate(r'\textit{$\tau_{id}$ = ' + str (np.round(total_ignition_delay, 2)) + ' ms}', \
            xy=((0.5 * (time_start_injection + time_start_ignition) - x_min) / (x_max - x_min) - 0.0841, 0.3375), xycoords='axes fraction', \
            xytext=((0.5 * (time_start_injection + time_start_ignition) - x_min) / (x_max - x_min) - 0.0841, 0.3375), textcoords='axes fraction', \
            fontsize = 8, color = '#303030', zorder = 7)
        if low_temperature_heat_relase == 1:
            pressure_vs_time.axvline(time_start_ignition_lthr, color = '#484848', linestyle = ':', linewidth = 0.75, zorder = 5)
            # pressure_vs_time.axvline(0.5 * (time_start_injection + time_start_ignition_lthr), color = '#484848', linestyle = ':', linewidth = 0.75, zorder = 2)
            pressure_vs_time.annotate('', xy = ((time_start_injection - x_min) / (x_max - x_min), 0.25), \
                xycoords = 'axes fraction', xytext = ((time_start_ignition_lthr - x_min) / (x_max - x_min), 0.25), \
                textcoords = 'axes fraction', fontsize = 7, color = '#484848', zorder = 7, arrowprops = dict(edgecolor='#484848', \
                facecolor='#484848', arrowstyle = '<|-|>', linewidth = 0.75, shrinkA = 0, shrinkB = 0))
            pressure_vs_time.annotate(r'\textit{$\tau_{id_{LTHR}}$ = ' + str (np.round(total_ignition_delay_lthr, 2)) + ' ms}', \
                xy=((0.5 * (time_start_injection + time_start_ignition_lthr) - x_min) / (x_max - x_min) - 0.11, 0.2575), xycoords='axes fraction', \
                xytext=((0.5 * (time_start_injection + time_start_ignition_lthr) - x_min) / (x_max - x_min) - 0.11, 0.2575), textcoords='axes fraction', \
                fontsize = 8, color = '#303030', zorder = 7)
        pressure_vs_time.legend((label_1, label_2, label_3), (label_1.get_label(), label_2.get_label(), \
            label_3.get_label()), title = r'\textit{$P_{charge}$ = ' + str(round(pressure_charge_initial, 2))  + \
            r'\textit{ bar}' + '\n' + \
            r'\textit{$P_{injection}$ = 225 bar}' + '\n' + \
            r'\textit{$P_{max}$ = ' + str(round(pressure_maximum, 2)) + r'\textit{ bar}' + '\n' + \
            r'\textit{${\left(\frac{dP}{dt}\right)}_{PR}$ = ' + str(round(gradient_pressure_recovery, 2)) + r'\textit{ bar/ms}' + '\n' + \
            r'\textit{${\left(\frac{dP}{dt}\right)}_{ign}$ = ' + str(round(gradient_pressure_rise_maximum, 2)) + r'\textit{ bar/ms}',
            loc = 'center left', bbox_to_anchor = (1.25, 0.5))
        pressure_vs_time.get_legend().get_title().set_size(10)
        square_plot(needle_vs_time)
        pressure_time.savefig(bbox_inches='tight')
        plt.close()
        pdf_metadata(pressure_time)
    logging.info(u'\u2714\t\t\t\t\t\u27A4 Generated figure of cylinder pressure versus crank angle degrees')
    # sys.exit()


# -----------------------------------------------------------------------------------
# Write result to file
# -----------------------------------------------------------------------------------
    result.writelines(str(local_counter) + '\t' + 
        str(data_file[1]) + '\t' + \
        str(round(pressure_charge_initial, 2)) + '\t' + 
        ('Yes' if low_temperature_heat_relase == 1 else 'No') + '\t' + \
        str(round(total_ignition_delay, 2)) + '\t' + \
        (str(round(total_ignition_delay_lthr, 2)) if low_temperature_heat_relase == 1 else '-') + '\t' + 
        (str(round(percentage_low_temperature_ignition_delay, 2)) if low_temperature_heat_relase == 1 else '-') + '\t' + \
        (str(round(duration_lthr, 2)) if low_temperature_heat_relase == 1 else '-') + '\t' + \
        (str(round(percentage_duration_lthr, 2)) if low_temperature_heat_relase == 1 else '-') + '\t' + \
        str(round(pressure_ignition, 2)) + '\t' + \
        str(round(percentage_pressure_ignition, 2)) + '\t' + \
        (str(round(pressure_lthr_ignition, 2)) if low_temperature_heat_relase == 1 else '-') + '\t' + \
        (str(round(percentage_pressure_lthr_ignition, 2)) if low_temperature_heat_relase == 1 else '-') + '\t' + \
        str(round(pressure_maximum, 2)) + '\t' + \
        str(round(percentage_pressure_maximum, 2)) + '\t' + \
        str(round(delay_pressure_maximum, 2)) + '\t' + \
        str(round(percentage_delay_pressure_maximum, 2)) + '\t' + \
        str(round(pressure_minimum, 2)) + '\t' + \
        str(round(percentage_pressure_minimum, 2)) + '\t' + \
        str(round(delay_pressure_minimum, 2)) + '\t' + \
        str(round(percentage_delay_pressure_minimum, 2)) + '\t' + \
        str(round(delay_pressure_recovery, 2)) + '\t' + \
        str(round(percentage_delay_pressure_recovery, 2)) + '\t' + \
        str(round(gradient_pressure_recovery, 2)) + '\t' + \
        str(round(delay_pressure_rise_maximum_gradient, 2)) + '\t' + \
        str(round(percentage_delay_gradient_pressure_rise_maximum, 2)) + '\t' + \
        str(round(gradient_pressure_rise_maximum, 2)) + '\t' + \
        str(round(burning_rate, 2)) + '\t' + \
        str(round(burning_duration, 2)) + '\n')
    local_counter = local_counter + 1


# -----------------------------------------------------------------------------------
# Close result file
# -----------------------------------------------------------------------------------
    if np.where(data_files[:, 2] == data_file[2])[0] == len(data_files) - 1 or data_file[0] != data_files[np.argwhere(data_files[:, 2] == data_file[2])[0] + 1, 0]:
        local_counter = 1
        initial_loop = 1
        with open(result.name, "r") as result:
            while True:
                line = result.readline()
                if not line.startswith('#'):
                    break
            res_header = [i for i in line.strip().split('\t')]
            _result_ = np.genfromtxt(result, names = res_header, dtype = None, delimiter = '\t')
            _result_.dtype.names = [j.replace('_', ' ') for j in _result_.dtype.names]
        serial_number = _result_['Serial number ']
        injection_number = _result_['Injection number ']
        pressure_charge_initial = _result_['Charge test pressure bar']
        low_temperature_heat_relase = _result_['Low temperature ignition']
        total_ignition_delay = _result_['Total ignition delay ms']
        total_ignition_delay_lthr = _result_['Low temperature ignition delay ms']
        percentage_low_temperature_ignition_delay = _result_['Percentage low temperature ignition delay ']
        duration_lthr = _result_['Low temperature heat release duration ms']
        percentage_duration_lthr = _result_['Percentage low temperature heat release duration ']
        pressure_ignition = _result_['Ignition pressure bar']
        percentage_pressure_ignition = _result_['Percentage ignition pressure ']
        pressure_lthr_ignition = _result_['Low temperature ignition pressure bar']
        percentage_pressure_lthr_ignition = _result_['Percentage low temperature ignition pressure ']
        pressure_maximum = _result_['Maximum charge pressure bar']
        percentage_pressure_maximum = _result_['Percentage maximum charge pressure ']
        delay_pressure_maximum = _result_['Maximum charge pressure delay ms']
        percentage_delay_pressure_maximum = _result_['Percentage maximum charge pressure delay ']
        pressure_minimum = _result_['Minimum charge pressure bar']
        percentage_pressure_minimum = _result_['Percentage minimum charge pressure ']
        delay_pressure_minimum = _result_['Minimum charge pressure delay ms']
        percentage_delay_pressure_minimum = _result_['Percentage minimum charge pressure delay ']
        delay_pressure_recovery = _result_['Pressure recovery delay ms']
        percentage_delay_pressure_recovery = _result_['Percentage pressure recovery delay ']
        gradient_pressure_recovery = _result_['Pressure recovery gradient barms']
        delay_pressure_rise_maximum_gradient = _result_['Pressure rise maximum gradient delay ms']
        percentage_delay_gradient_pressure_rise_maximum = _result_['Percentage pressure rise maximum gradient delay ']
        gradient_pressure_rise_maximum = _result_['Maximum gradient during ignition barms']
        burning_rate = _result_['Burning rate barms']
        burning_duration = _result_['Burning duration ms']          


# -----------------------------------------------------------------------------------
# Representative cycle determination
# -----------------------------------------------------------------------------------
        if serial_number.size != 1:
            injection_represntative_cycle = injection_number[cycle_match(total_ignition_delay, np.mean(total_ignition_delay))]
        else:
            injection_represntative_cycle = injection_number
        result_representative_cycle = open('./Processed data/SAT - Result - ' + data_file[0] + \
            '/Processed pressure traces/SAT - Result - Processed pressure - ' + data_file[0] + ' - ' + \
            str(injection_represntative_cycle).rjust(3, '0') +'.dat','r')


# -----------------------------------------------------------------------------------
# Create processed pressure file
# -----------------------------------------------------------------------------------
        result_representative_injection_pressure = open('./Processed data/SAT - Result - ' + data_file[0] + \
            '/Representative injection/SAT - Result - ' + data_file[0] + ' - ' + \
            str(injection_represntative_cycle).rjust(3, '0') +' - Processed pressure.dat','w')
        result_representative_injection_pressure.write('#============================================================================================================\n')
        result_representative_injection_pressure.write('# Spray Autoignition Tool (SAT)\n')
        result_representative_injection_pressure.write('#============================================================================================================\n')
        result_representative_injection_pressure.write('#\n')
        result_representative_injection_pressure.write('# Author                  : Nimal Naser (nimal.naser@gmail.com, nimal.naser@kaust.edu.sa)\n')
        result_representative_injection_pressure.write('# Institute               : King Abdullah University of Science and Technology (KAUST)\n')
        result_representative_injection_pressure.write('# Department              : Mechanical Engineering\n')
        result_representative_injection_pressure.write('# Research center         : Clean Combustion Research Center (CCRC)\n')
        result_representative_injection_pressure.write('# Instrument/Device       : KAUST Research - Ignition Quality Tester (KR-IQT)\n')
        result_representative_injection_pressure.write('# Data processing date    : ' + str(datetime.today().strftime("%Y-%m-%d %H:%M:%S")) + '\n')
        result_representative_injection_pressure.write('# Chamber volume          : 0.00021 m^3\n')
        result_representative_injection_pressure.write('# Injection pressure      : 225 bar\n')
        result_representative_injection_pressure.write('# Ignition determination  : Gradient method\n')
        result_representative_injection_pressure.write('#\n')
        result_representative_injection_pressure.write('# -----------------------------------------------------------------------------------\n')
        result_representative_injection_pressure.write('# Processed pressure of data set ' + str(data_file[0]) + ' - ' + str(injection_represntative_cycle).rjust(3, '0') +'\n')
        result_representative_injection_pressure.write('# -----------------------------------------------------------------------------------\n')
        result_representative_injection_pressure.write('# 1. Time (ms) | 2. Raw pressure (bar) | 3. Smoothened pressure (bar) 4. | Needle lift (ms)\n')
        result_representative_injection_pressure.writelines('Time\tRaw pressure\tSmoothened pressure\tNeedle lift\n')
        # result_representative_injection_pressure.writelines('(\tRaw pressure\tSmoothened pressure\tNeedle lift\n')

        with open(result_representative_cycle.name, "r") as result_representative_cycle:
            while True:
                line = result_representative_cycle.readline()
                if not line.startswith('#'):
                    break
            res_header = [i for i in line.strip().split('\t')]
            _result_ = np.genfromtxt(result_representative_cycle, names = res_header, dtype = None, delimiter = '\t')
            _result_.dtype.names = [j.replace('_', ' ') for j in _result_.dtype.names]


# -----------------------------------------------------------------------------------
# Calculate sampling interval and determine indices where sampling interval changes
# -----------------------------------------------------------------------------------
        sampling_interval = np.around(np.diff(time), 2)                             # Determine sampling interval
        sampling_interval_min = round(np.amin(sampling_interval), 2)                # Determine the minimum sampling interval
        dt = sampling_interval_min / 1000
        sampling_rate = 1 / dt          # Sampling rate (in Hz)
        logging.info(u'\u2714\t\t\t\t\t\u27A4 Calculated sampling interval and sampling rate')


# -----------------------------------------------------------------------------------
# Filtering
# -----------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------
# Filter design
# -----------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------
# Power spectral density calculations before applying filter
# -----------------------------------------------------------------------------------
        power_real, frequency = plt.psd(pressure * 100000, NFFT = int(nearest_power(len(pressure), 2, '+')), Fs = 1 / dt, scale_by_freq = False)


# -----------------------------------------------------------------------------------
# Convert power spectral density to decibel
# -----------------------------------------------------------------------------------
        power = 10 * np.log10(power_real)


# -----------------------------------------------------------------------------------
# Apply filter to power spectral density
# -----------------------------------------------------------------------------------
        power_filtered = sm.nonparametric.lowess(power, frequency, frac=0.004)[:,1]
        d_power_filtered_d_frequency = savitzky_golay(power_filtered, window_size = 101, order = 9, deriv = 1)


# -----------------------------------------------------------------------------------
# Calculate cut-off frequencies
# -----------------------------------------------------------------------------------
        for i in range (10, len(power)):
            if 0 <= np.round(d_power_filtered_d_frequency[i], 3):
                cut_off_frequency = frequency[i]
                break

        logging.info(u'\u2714\t\t\t\t\t\u27A4 Obtained cut-off frequency for combustion chamber pressure curve')
        cut_off_frequency = 1500


# -----------------------------------------------------------------------------------
# Plot power spectral density of cylinder pressure (combustion)
# -----------------------------------------------------------------------------------
        with PdfPages('./Processed data/SAT - Result - ' + data_file[0] + '/Representative injection/SAT - Result - ' + data_file[0] + \
            ' - ' + str(injection_represntative_cycle).rjust(3, '0') + ' - Power spectral density of combustion chamber pressure.pdf') as power_spectral_density_pressure:
            _power_spectral_density_pressure_vs_frequency_ = plt.figure(figsize=(5, 5))
            _power_spectral_density_pressure_vs_frequency_.clf()
            power_spectral_density_pressure_vs_frequency = plt.subplot(211)
            power_spectral_density_pressure_vs_frequency.plot(frequency, power, label = r'\textit{Raw}')
            power_spectral_density_pressure_vs_frequency.plot(frequency, power_filtered, label = r'\textit{Filtered}')
            power_spectral_density_pressure_vs_frequency.axvline(x=cut_off_frequency, linewidth=0.5, linestyle=':', color='r')
            plt.xscale('log')
            power_spectral_density_pressure_vs_frequency.grid(True, which="major", lw=0.5)
            power_spectral_density_pressure_vs_frequency.grid(True, which="minor", lw=0.1)
            power_spectral_density_pressure_vs_frequency.set_xlabel(r'\textit{Frequency (Hz)}', labelpad = 6)
            power_spectral_density_pressure_vs_frequency.set_ylabel(r'\textit{{Power spectral density (dB)}}', labelpad = 6)
            power_spectral_density_pressure_vs_frequency.legend(loc = 'center left', bbox_to_anchor = (1.025, 0.85))
            power_spectral_density_pressure_vs_frequency = plt.subplot(212)
            power_spectral_density_pressure_vs_frequency.plot(frequency, d_power_filtered_d_frequency, label = r'\textit{}')
            # power_spectral_density_pressure_vs_frequency.plot(frequency, power_filtered, label = r'\textit{Filtered}')
            power_spectral_density_pressure_vs_frequency.axvline(x=cut_off_frequency, linewidth=0.5, linestyle=':', color='r')
            plt.xscale('log')
            power_spectral_density_pressure_vs_frequency.grid(True, which="major", lw=0.5)
            power_spectral_density_pressure_vs_frequency.grid(True, which="minor", lw=0.1)
            power_spectral_density_pressure_vs_frequency.set_xlabel(r'\textit{Frequency (Hz)}', labelpad = 6)
            power_spectral_density_pressure_vs_frequency.set_ylabel(r'\textit{{$\frac{d(Power spectral density)}{d\theta}$}}', labelpad = 6)
            power_spectral_density_pressure_vs_frequency.legend(loc = 'center left', bbox_to_anchor = (1.025, 0.85))
            plt.tight_layout()
            power_spectral_density_pressure.savefig(bbox_inches='tight')
            plt.close()
            pdf_metadata(power_spectral_density_pressure)
        logging.info(u'\u2714\t\t\t\t\t\u27A4 Generated figure of power spectral density of combustion chamber pressure')   


# -----------------------------------------------------------------------------------
# Apply low pass butterworth filter on cylinder pressure
# -----------------------------------------------------------------------------------
        pressure_smooth = sm.nonparametric.lowess(pressure, time, is_sorted=True, frac=0.04, it=0)[:,1]


# -----------------------------------------------------------------------------------
# Write processed pressure to result file
# -----------------------------------------------------------------------------------
        np.savetxt(result_representative_injection_pressure, np.column_stack((time, pressure, pressure_smooth, needle_lift)), fmt='%.2f', delimiter='\t')


# -----------------------------------------------------------------------------------
# Determine start of injection
# -----------------------------------------------------------------------------------
        i_needle_lift_maximum = np.min(np.argwhere(needle_lift == np.max(needle_lift)))
        time_start_injection = time[i_needle_lift_maximum]


# -----------------------------------------------------------------------------------
# Initial chamber pressure
# -----------------------------------------------------------------------------------
        _pressure_charge_initial_ = np.mean(pressure[0: i_needle_lift_maximum])


# -----------------------------------------------------------------------------------
# Determine pressure recovery point
# -----------------------------------------------------------------------------------
        i_pressure_recovery = i_needle_lift_maximum + bisect(pressure_smooth[i_needle_lift_maximum:-1], _pressure_charge_initial_)
        time_pressure_recovery = time[i_pressure_recovery]
        pressure_recovery = pressure_smooth[i_pressure_recovery]


# -----------------------------------------------------------------------------------
# Maximum chamber pressure
# -----------------------------------------------------------------------------------
        for i in range(i_pressure_recovery, len(pressure_smooth)):
            slope = (pressure_smooth[i + 1] - pressure_smooth[i]) / (time[i + 1] - time[i])
            if slope <= 0:
                i_pressure_approximate_maximum = i
                break
        i__pressure_maximum_ = np.min(np.argwhere(pressure_smooth == np.max(pressure_smooth[0:i_pressure_approximate_maximum])))
        _pressure_maximum_ = pressure_smooth[i__pressure_maximum_]
        time__pressure_maximum_ = time[i__pressure_maximum_]


# -----------------------------------------------------------------------------------
# Minumum chamber pressure
# -----------------------------------------------------------------------------------
        i__pressure_minimum_ = np.min(np.argwhere(pressure_smooth == np.min(pressure_smooth[i_needle_lift_maximum:i__pressure_maximum_])))
        _pressure_minimum_ = pressure_smooth[i__pressure_minimum_]
        time__pressure_minimum_ = time[i__pressure_minimum_]

# -----------------------------------------------------------------------------------
# Slope at pressure recovery point
# -----------------------------------------------------------------------------------
        _gradient_pressure_recovery_ = (pressure_smooth[i_pressure_recovery + 1] - pressure_smooth[i_pressure_recovery]) \
            / (time[i_pressure_recovery + 1] - time[i_pressure_recovery])


# -----------------------------------------------------------------------------------
# Determine gradient of pressure signal
# -----------------------------------------------------------------------------------
        gradient_pressure = np.zeros(shape = (len(pressure_smooth), ))
        for i in range(len(gradient_pressure) - 1):
            gradient_pressure[i] = (pressure_smooth[i + 1] - pressure_smooth[i]) \
                / (time[i + 1] - time[i])
        gradient_pressure_smooth = savitzky_golay(gradient_pressure, 11, 1)
        dslope_dt = np.gradient(gradient_pressure_smooth)
        dslope_dt_smooth = savitzky_golay(dslope_dt, 11, 1)
        i__gradient_pressure_rise_maximum_ = np.min(np.argwhere(gradient_pressure_smooth == np.max(gradient_pressure_smooth[i_pressure_recovery:i__pressure_maximum_])))
        _gradient_pressure_rise_maximum_ = gradient_pressure[i__gradient_pressure_rise_maximum_]
        pressure__gradient_pressure_rise_maximum_ = pressure_smooth[i__gradient_pressure_rise_maximum_]
        time_pressure_rise_maximum_gradient = time[i__gradient_pressure_rise_maximum_]


# -----------------------------------------------------------------------------------
# Determine start of ignition
# -----------------------------------------------------------------------------------
        gradients_intersection = np.linalg.solve(np.array([[_gradient_pressure_rise_maximum_, -1], [_gradient_pressure_recovery_, -1]]), \
            np.array([_gradient_pressure_rise_maximum_ * time_pressure_rise_maximum_gradient - pressure__gradient_pressure_rise_maximum_, \
            _gradient_pressure_recovery_ * time_pressure_recovery - pressure_recovery]))
        i_ignition = i_pressure_recovery + bisect(time[i_pressure_recovery:i__pressure_maximum_], gradients_intersection[0])
        time_start_ignition = time[i_ignition]
        _pressure_ignition_ = pressure_smooth[i_ignition]


# -----------------------------------------------------------------------------------
# Slope line at pressure recovery point
# -----------------------------------------------------------------------------------
        x_pressure_recovery = np.linspace(time_pressure_recovery, time[-1], 100)
        y_pressure_recovery = pressure_recovery + _gradient_pressure_recovery_ * (x_pressure_recovery - time_pressure_recovery)


# -----------------------------------------------------------------------------------
# Slope line at ignition
# -----------------------------------------------------------------------------------
        y_pressure_ignition = np.linspace(0, pressure__gradient_pressure_rise_maximum_, 100)
        x_pressure_ignition = time_pressure_rise_maximum_gradient + (y_pressure_ignition - pressure__gradient_pressure_rise_maximum_) / _gradient_pressure_rise_maximum_


# -----------------------------------------------------------------------------------
# Burn rate and duration
# -----------------------------------------------------------------------------------
        for i in range(i_ignition, i__pressure_maximum_):
            if pressure_smooth[i] >= 0.8 * _pressure_ignition_ + 0.2 * _pressure_maximum_:
                i_burn_start = i
                break
        for i in range(i_ignition, i__pressure_maximum_):
            if pressure_smooth[i] >= 0.8 * _pressure_maximum_ + 0.2 * _pressure_ignition_:
                i_burn_end = i
                break
        _burning_rate_ = (pressure_smooth[i_burn_end] - pressure_smooth[i_burn_start]) / (time[i_burn_end] - time[i_burn_start])
        _burning_duration_ = time[i_burn_end] - time[i_burn_start]


# -----------------------------------------------------------------------------------
# Low temperature heat release
# -----------------------------------------------------------------------------------
        i_lthr_inflection = []
        for i in (range(i_pressure_recovery, i_ignition)):
            if dslope_dt_smooth[i] > 0 and dslope_dt_smooth[i + 1] < 0:
                i_lthr_inflection = np.append(i_lthr_inflection, i)
        # i_lthr_inflection = np.int64(i_lthr_inflection)
        if i_lthr_inflection.size:
            i_slope_low_temperature_heat_release_minimum = np.min(np.argwhere(gradient_pressure_smooth == np.min(gradient_pressure_smooth[i_lthr_inflection[0]:i_ignition])))
            i_slope_low_temperature_heat_release_maximum = np.min(np.argwhere(gradient_pressure_smooth == np.max(gradient_pressure_smooth[i_pressure_recovery:i_lthr_inflection[0]])))
            gradient_lthr_minimum = (pressure_smooth[i_slope_low_temperature_heat_release_minimum + 1] - \
                pressure_smooth[i_slope_low_temperature_heat_release_minimum]) / \
                (time[i_slope_low_temperature_heat_release_minimum + 1] - time[i_slope_low_temperature_heat_release_minimum])
            gradient_lthr_maximum = (pressure_smooth[i_slope_low_temperature_heat_release_maximum + 1] - \
                pressure_smooth[i_slope_low_temperature_heat_release_maximum]) / \
                (time[i_slope_low_temperature_heat_release_maximum + 1] - time[i_slope_low_temperature_heat_release_maximum])
            time_lthr_minimum = time[i_slope_low_temperature_heat_release_minimum]
            time_lthr_maximum = time[i_slope_low_temperature_heat_release_maximum]
            pressure_lthr_minimum = pressure_smooth[i_slope_low_temperature_heat_release_minimum]
            pressure_lthr_maximum = pressure_smooth[i_slope_low_temperature_heat_release_maximum]


# -----------------------------------------------------------------------------------
# Determine start of ignition
# -----------------------------------------------------------------------------------
            lthr_gradients_intersection = np.linalg.solve(np.array([[gradient_lthr_maximum, -1], [gradient_lthr_minimum, -1]]), \
                np.array([gradient_lthr_maximum * time_lthr_maximum - pressure_lthr_maximum, \
                gradient_lthr_minimum * time_lthr_minimum - pressure_lthr_minimum]))
            i_lthr_ignition_gradient_method = i_pressure_recovery + bisect(time[i_pressure_recovery:i__pressure_maximum_], lthr_gradients_intersection[0])
            time_lthr_ignition_gradient_method = time[i_lthr_ignition_gradient_method]
            _pressure_lthr_ignition__gradient_method = pressure_smooth[i_lthr_ignition_gradient_method]
            if pressure_recovery <= _pressure_lthr_ignition__gradient_method <= _pressure_ignition_:
                low_temperature_heat_relase = 1
            else:
                low_temperature_heat_relase = 0


# ------------------------------------------------------------------------------------------------------------
# Slope line at pressure recovery point
# ------------------------------------------------------------------------------------------------------------
        if low_temperature_heat_relase == 1:
            i_lthr_ignition_iqt_method = i_pressure_recovery + bisect(pressure_smooth[i_pressure_recovery:-1], _pressure_charge_initial_ + 1.38)
            time_lthr_ignition_iqt_method = time[i_lthr_ignition_iqt_method]
            _pressure_lthr_ignition__iqt_method = pressure[i_lthr_ignition_iqt_method]
            if time_lthr_ignition_gradient_method < time_lthr_ignition_iqt_method:
                method_start_lthr_ignition = 'Gradient method'
                time_start_ignition_lthr = time_lthr_ignition_gradient_method
                _pressure_lthr_ignition_ = _pressure_lthr_ignition__gradient_method
                x_low_temperature_heat_release_minimum = np.linspace(time_lthr_ignition_gradient_method, time_lthr_minimum, 100)
                y_low_temperature_heat_release_minimum = pressure_lthr_minimum + \
                    gradient_lthr_minimum * (x_low_temperature_heat_release_minimum - time_lthr_minimum)


# -----------------------------------------------------------------------------------
# Slope line at ignition
# -----------------------------------------------------------------------------------
                x_low_temperature_heat_release_maximum = np.linspace(time_lthr_maximum, time_lthr_ignition_gradient_method, 100)
                y_low_temperature_heat_release_maximum = pressure_lthr_maximum + \
                    gradient_lthr_maximum * (x_low_temperature_heat_release_maximum - time_lthr_maximum)
            else:
                method_start_lthr_ignition = 'IQT method'       
                time_start_ignition_lthr = time_lthr_ignition_iqt_method
                _pressure_lthr_ignition_ = _pressure_charge_initial_ + 1.38



# -----------------------------------------------------------------------------------
# Ignition delay calculations
# -----------------------------------------------------------------------------------
        _total_ignition_delay_ = time_start_ignition - time_start_injection


# -----------------------------------------------------------------------------------
# Other calculations
# -----------------------------------------------------------------------------------
        _delay_pressure_recovery_ = time_pressure_recovery - time_start_injection
        _delay_pressure_minimum_ = time__pressure_minimum_ - time_start_injection
        _delay_pressure_rise_maximum_gradient_ = time_pressure_rise_maximum_gradient - time_start_injection
        _delay_pressure_maximum_ = time__pressure_maximum_ - time_start_injection


# -----------------------------------------------------------------------------------
# Percentage calculations
# -----------------------------------------------------------------------------------
        _percentage_pressure_ignition_ = ((_pressure_ignition_ - _pressure_charge_initial_) / _pressure_charge_initial_) * 100
        _percentage_pressure_maximum_ = ((_pressure_maximum_ - _pressure_charge_initial_) / _pressure_charge_initial_) * 100
        percentage__pressure_minimum_ = ((_pressure_minimum_ - _pressure_charge_initial_) / _pressure_charge_initial_) * 100
        percentage_pressure__gradient_pressure_rise_maximum_ = ((pressure__gradient_pressure_rise_maximum_ - _pressure_charge_initial_) / _pressure_charge_initial_) * 100
        _percentage_delay_pressure_recovery_ = (_delay_pressure_recovery_ / _total_ignition_delay_) * 100
        _percentage_delay_pressure_minimum_ = (_delay_pressure_minimum_ / _total_ignition_delay_) * 100
        _percentage_delay_pressure_maximum_ = (_delay_pressure_maximum_ / _total_ignition_delay_) * 100
        _percentage_delay_gradient_pressure_rise_maximum_ = (_delay_pressure_rise_maximum_gradient_ / _total_ignition_delay_) * 100


# -----------------------------------------------------------------------------------
# Low temperature calculations
# -----------------------------------------------------------------------------------
        if low_temperature_heat_relase == 1:
            _total_ignition_delay__lthr = time_start_ignition_lthr - time_start_injection
            _duration_lthr_ = _total_ignition_delay_ - _total_ignition_delay__lthr
            _percentage_low_temperature_ignition_delay_ = (_total_ignition_delay__lthr / _total_ignition_delay_) * 100
            _percentage_duration_lthr_ = (_duration_lthr_ / _total_ignition_delay_) * 100
            _percentage_pressure_lthr_ignition_ = ((_pressure_lthr_ignition_ - _pressure_charge_initial_) / _pressure_charge_initial_) * 100


# -----------------------------------------------------------------------------------
# Plot cylinder pressure versus crank angle degrees
# -----------------------------------------------------------------------------------
        with PdfPages('./Processed data/SAT - Result - ' + data_file[0] + '/Representative injection/SAT - Result - ' + data_file[0] + \
            ' - ' + str(injection_represntative_cycle).rjust(3, '0') + ' - Pressure trace.pdf') as pressure_time:
            _pressure_vs_time_ = plt.figure(figsize=(5, 5))
            _pressure_vs_time_.clf()
            needle_vs_time = plt.subplot(111)
            label_1, = needle_vs_time.plot(time, needle_lift, color='#4DAF4A', label = r'\textit{Needle lift}', zorder = 1)
            needle_vs_time.set_xlabel(r'\textit{Time (ms)}', labelpad = 6)
            needle_vs_time.set_ylabel(r'\textit{Needle lift (mm)}', labelpad = 6)
            pressure_vs_time = needle_vs_time.twinx()
            label_2, = pressure_vs_time.plot(time, pressure, label = r'\textit{Raw}', zorder = 3)
            label_3, = pressure_vs_time.plot(time, pressure_smooth, label = r'\textit{Filtered}', zorder = 4)           
            pressure_vs_time.set_ylabel(r'\textit{Combustion chamber pressure (MPa)}', labelpad = 6)
            pressure_vs_time.axvline(time_start_injection, color = '#484848', linestyle = ':', linewidth = 0.75, zorder = 6)
            pressure_vs_time.axvline(time_start_ignition, color = '#484848', linestyle = ':', linewidth = 0.75, zorder = 6)
            # pressure_vs_time.axhline(_pressure_ignition_, color = '#484848', linestyle = ':', linewidth = 0.75, zorder = 6)

            # pressure_vs_time.axvline(time[i__pressure_minimum_], color = '#484848', linestyle = ':', linewidth = 0.75, zorder = 6)

            # pressure_vs_time.axvline(0.5 * (time_start_injection + time_start_ignition), color = '#484848', linestyle = ':', linewidth = 0.75, zorder = 2)
            pressure_vs_time.axhline(_pressure_charge_initial_, color = '#484848', linestyle = ':', linewidth = 0.75, zorder = 5)
            pressure_vs_time.plot(x_pressure_recovery, y_pressure_recovery, linewidth = 0.75, color = '#484848', zorder = 2)
            pressure_vs_time.plot(x_pressure_ignition, y_pressure_ignition, linewidth = 0.75, color = '#484848', zorder = 2)
            pressure_vs_time.grid(False)
            pressure_vs_time.set_ylim(bottom = 0)
            primary_ticks = len(pressure_vs_time.yaxis.get_major_ticks())
            needle_vs_time.yaxis.set_major_locator(mtick.LinearLocator(primary_ticks))
            needle_vs_time.yaxis.tick_right()
            needle_vs_time.yaxis.set_label_position("right")
            pressure_vs_time.yaxis.tick_left()
            pressure_vs_time.yaxis.set_label_position("left")           
            y_min, y_max = pressure_vs_time.get_ylim()
            x_min, x_max = pressure_vs_time.get_xlim()
            pressure_vs_time.annotate(r'\textit{$P_{initial}$ = ' + str(np.round(_pressure_charge_initial_, 2)) + ' bar}', \
                (0.77, (_pressure_charge_initial_ - y_min) / (y_max - y_min) + 0.0075), color = '#000000', textcoords = 'axes fraction', \
                fontsize = 8, zorder = 7)
            pressure_vs_time.annotate('', xy = ((time_start_injection - x_min) / (x_max - x_min), 0.33), \
                xycoords = 'axes fraction', xytext = ((time_start_ignition - x_min) / (x_max - x_min), 0.33), \
                textcoords = 'axes fraction', fontsize = 7, color = '#484848', zorder = 7, arrowprops = dict(edgecolor='#484848', \
                facecolor='#484848', arrowstyle = '<|-|>', linewidth = 0.75, shrinkA = 0, shrinkB = 0))
            pressure_vs_time.annotate(r'\textit{$\tau_{id}$ = ' + str (np.round(_total_ignition_delay_, 2)) + ' ms}', \
                xy=((0.5 * (time_start_injection + time_start_ignition) - x_min) / (x_max - x_min) - 0.0841, 0.3375), xycoords='axes fraction', \
                xytext=((0.5 * (time_start_injection + time_start_ignition) - x_min) / (x_max - x_min) - 0.0841, 0.3375), textcoords='axes fraction', \
                fontsize = 8, color = '#303030', zorder = 7)
            if low_temperature_heat_relase == 1:
                pressure_vs_time.axvline(time_start_ignition_lthr, color = '#484848', linestyle = ':', linewidth = 0.75, zorder = 5)
                # pressure_vs_time.axvline(0.5 * (time_start_injection + time_start_ignition_lthr), color = '#484848', linestyle = ':', linewidth = 0.75, zorder = 2)
                pressure_vs_time.annotate('', xy = ((time_start_injection - x_min) / (x_max - x_min), 0.25), \
                    xycoords = 'axes fraction', xytext = ((time_start_ignition_lthr - x_min) / (x_max - x_min), 0.25), \
                    textcoords = 'axes fraction', fontsize = 7, color = '#484848', zorder = 7, arrowprops = dict(edgecolor='#484848', \
                    facecolor='#484848', arrowstyle = '<|-|>', linewidth = 0.75, shrinkA = 0, shrinkB = 0))
                pressure_vs_time.annotate(r'\textit{$\tau_{id_{LTHR}}$ = ' + str (np.round(_total_ignition_delay_, 2)) + ' ms}', \
                    xy=((0.5 * (time_start_injection + time_start_ignition_lthr) - x_min) / (x_max - x_min) - 0.11, 0.2575), xycoords='axes fraction', \
                    xytext=((0.5 * (time_start_injection + time_start_ignition_lthr) - x_min) / (x_max - x_min) - 0.11, 0.2575), textcoords='axes fraction', \
                    fontsize = 8, color = '#303030', zorder = 7)
            pressure_vs_time.legend((label_1, label_2, label_3), (label_1.get_label(), label_2.get_label(), \
                label_3.get_label()), title = r'\textit{$P_{charge}$ = ' + str(round(_pressure_charge_initial_, 2))  + r'\textit{ bar}' + '\n' + \
                r'\textit{$P_{injection}$ = 225 bar}' + '\n' + \
                r'\textit{$P_{max}$ = ' + str(round(_pressure_maximum_, 2)) + r'\textit{ bar}' + '\n' + \
                r'\textit{${\left(\frac{dP}{dt}\right)}_{PR}$ = ' + str(round(_gradient_pressure_recovery_, 2)) + r'\textit{ bar/ms}' + '\n' + \
                r'\textit{${\left(\frac{dP}{dt}\right)}_{ign}$ = ' + str(round(_gradient_pressure_rise_maximum_, 2)) + r'\textit{ bar/ms}',
                loc = 'center left', bbox_to_anchor = (1.25, 0.5))
            pressure_vs_time.get_legend().get_title().set_size(10)
            square_plot(needle_vs_time)
            pressure_time.savefig(bbox_inches='tight')
            plt.close()
            pdf_metadata(pressure_time)
        logging.info(u'\u2714\t\t\t\t\t\u27A4 Generated figure of cylinder pressure versus crank angle degrees')
        # sys.exit()


# -----------------------------------------------------------------------------------
# Write to result file mean and standard deviation
# -----------------------------------------------------------------------------------
        # logging.info(u'\u2714\t\t\t\t\t\u27A4 Generated time, combustion chamber pressure, needle lift array')
        result = open('./Processed data/SAT - Result - ' + data_file[0] + '/SAT - Result - ' + data_file[0] + '.dat','a')
        if len(np.atleast_1d(total_ignition_delay)) > 1:
            result.write('--------------------------------------------------------------------------------------------------------------\n')
            result.writelines('Mean' + '\t\t' + \
                str(round(np.mean(pressure_charge_initial), 2)) + '\t' + \
                (str('Yes')  if low_temperature_heat_relase == 1 else str('No')) + '\t' + \
                str(round(np.mean(total_ignition_delay), 2)) + '\t' + \
                (str(round(np.mean(total_ignition_delay_lthr), 2)) if low_temperature_heat_relase == 1 else '-') + '\t' + \
                (str(round(np.mean(percentage_low_temperature_ignition_delay), 2)) if low_temperature_heat_relase == 1 else '-') + '\t' + \
                (str(round(np.mean(duration_lthr), 2)) if low_temperature_heat_relase == 1 else '-') + '\t' + \
                (str(round(np.mean(percentage_duration_lthr), 2)) if low_temperature_heat_relase == 1 else '-') + '\t' + \
                str(round(np.mean(pressure_ignition), 2)) + '\t' + \
                str(round(np.mean(percentage_pressure_ignition), 2)) + '\t' + \
                (str(round(np.mean(pressure_lthr_ignition), 2)) if low_temperature_heat_relase == 1 else '-') + '\t' + \
                (str(round(np.mean(percentage_pressure_lthr_ignition), 2)) if low_temperature_heat_relase == 1 else '-') + '\t' + \
                str(round(np.mean(pressure_maximum), 2)) + '\t' + \
                str(round(np.mean(percentage_pressure_maximum), 2)) + '\t' + \
                str(round(np.mean(delay_pressure_maximum), 2)) + '\t' + \
                str(round(np.mean(percentage_delay_pressure_maximum), 2)) + '\t' + \
                str(round(np.mean(pressure_minimum), 2)) + '\t' + \
                str(round(np.mean(percentage_pressure_minimum), 2)) + '\t' + \
                str(round(np.mean(delay_pressure_minimum), 2)) + '\t' + \
                str(round(np.mean(percentage_delay_pressure_minimum), 2)) + '\t' + \
                str(round(np.mean(delay_pressure_recovery), 2)) + '\t' + \
                str(round(np.mean(percentage_delay_pressure_recovery), 2)) + '\t' + \
                str(round(np.mean(gradient_pressure_recovery), 2)) + '\t' + \
                str(round(np.mean(delay_pressure_rise_maximum_gradient), 2)) + '\t' + \
                str(round(np.mean(percentage_delay_gradient_pressure_rise_maximum), 2)) + '\t' + \
                str(round(np.mean(gradient_pressure_rise_maximum), 2)) + '\t' + \
                str(round(np.mean(burning_rate), 2)) + '\t' + \
                str(round(np.mean(burning_duration), 2)) + '\n')
            result.writelines('SD' + '\t\t' + \
                str(round(np.std(pressure_charge_initial), 3)) + '\t\t' + \
                str(round(np.std(total_ignition_delay), 3)) + '\t' + \
                (str(round(np.std(total_ignition_delay_lthr), 3))if low_temperature_heat_relase == 1 else '-') + '\t' + \
                (str(round(np.std(percentage_low_temperature_ignition_delay), 3)) if low_temperature_heat_relase == 1 else '-') + '\t' + \
                (str(round(np.std(duration_lthr), 3)) if low_temperature_heat_relase == 1 else '-') + '\t' + \
                (str(round(np.std(percentage_duration_lthr), 3)) if low_temperature_heat_relase == 1 else '-') + '\t' + \
                str(round(np.std(pressure_ignition), 3)) + '\t' + \
                str(round(np.std(percentage_pressure_ignition), 3)) + '\t' + \
                (str(round(np.std(pressure_lthr_ignition), 3)) if low_temperature_heat_relase == 1 else '-') + '\t' + \
                (str(round(np.std(percentage_pressure_lthr_ignition), 3)) if low_temperature_heat_relase == 1 else '-') + '\t' + \
                str(round(np.std(pressure_maximum), 3)) + '\t' + \
                str(round(np.std(percentage_pressure_maximum), 3)) + '\t' + \
                str(round(np.std(delay_pressure_maximum), 3)) + '\t' + \
                str(round(np.std(percentage_delay_pressure_maximum), 3)) + '\t' + \
                str(round(np.std(pressure_minimum), 3)) + '\t' + \
                str(round(np.std(percentage_pressure_minimum), 3)) + '\t' + \
                str(round(np.std(delay_pressure_minimum), 3)) + '\t' + \
                str(round(np.std(percentage_delay_pressure_minimum), 3)) + '\t' + \
                str(round(np.std(delay_pressure_recovery), 3)) + '\t' + \
                str(round(np.std(percentage_delay_pressure_recovery), 3)) + '\t' + \
                str(round(np.std(gradient_pressure_recovery), 3)) + '\t' + \
                str(round(np.std(delay_pressure_rise_maximum_gradient), 3)) + '\t' + \
                str(round(np.std(percentage_delay_gradient_pressure_rise_maximum), 3)) + '\t' + \
                str(round(np.std(gradient_pressure_rise_maximum), 3)) + '\t' + \
                str(round(np.std(burning_rate), 3)) + '\t' + \
                str(round(np.std(burning_duration), 3)) + '\n')
            result.write('--------------------------------------------------------------------------------------------------------------\n')
            result.write('# Remarks: Average of ' + str(len(total_ignition_delay)) + ' injections.\n')
            result.write('# Smoothing method: Local regression using weighted linear least squares and a 2nd degree polynomial model.\n')

        else:
            result.write('--------------------------------------------------------------------------------------------------------------\n')
            result.write('# Remarks: Only 1 injection.\n')          
            result.write('# Smoothing method: Local regression using weighted linear least squares and a 2nd degree polynomial model.\n')


# -----------------------------------------------------------------------------------
# Write processed pressure to result file
# -----------------------------------------------------------------------------------
        result_all.write(str(global_counter) + '\t' + 'IQT' + '\t' + \
            str(data_file[0]) + '\t' + \
            str(round(np.mean(pressure_charge_initial), 2)) + '\t' + \
            str(round(np.mean(total_ignition_delay), 2)) + '\t' + \
            (str(round(np.mean(total_ignition_delay_lthr), 2)) if low_temperature_heat_relase == 1 else '-') + '\t' + \
            (str(round(np.mean(percentage_low_temperature_ignition_delay), 2)) if low_temperature_heat_relase == 1 else '-') + '\t' + \
            (str(round(np.mean(duration_lthr), 2)) if low_temperature_heat_relase == 1 else '-') + '\t' + \
            (str(round(np.mean(percentage_duration_lthr), 2)) if low_temperature_heat_relase == 1 else '-') + '\t' + \
            str(round(np.mean(pressure_ignition), 2)) + '\t' + \
            str(round(np.mean(percentage_pressure_ignition), 2)) + '\t' + \
            (str(round(np.mean(pressure_lthr_ignition), 2)) if low_temperature_heat_relase == 1 else '-') + '\t' + \
            (str(round(np.mean(percentage_pressure_lthr_ignition), 2)) if low_temperature_heat_relase == 1 else '-') + '\t' + \
            str(round(np.mean(pressure_maximum), 2)) + '\t' + \
            str(round(np.mean(percentage_pressure_maximum), 2)) + '\t' + \
            str(round(np.mean(delay_pressure_maximum), 2)) + '\t' + \
            str(round(np.mean(percentage_delay_pressure_maximum), 2)) + '\t' + \
            str(round(np.mean(pressure_minimum), 2)) + '\t' + \
            str(round(np.mean(percentage_delay_pressure_minimum), 2)) + '\t' + \
            str(round(np.mean(delay_pressure_minimum), 2)) + '\t' + \
            str(round(np.mean(percentage_delay_pressure_minimum), 2)) + '\t' + \
            str(round(np.mean(delay_pressure_recovery), 2)) + '\t' + \
            str(round(np.mean(percentage_delay_pressure_recovery), 2)) + '\t' + \
            str(round(np.mean(gradient_pressure_recovery), 2)) + '\t' + \
            str(round(np.mean(delay_pressure_rise_maximum_gradient), 2)) + '\t' + \
            str(round(np.mean(percentage_delay_gradient_pressure_rise_maximum), 2)) + '\t' + \
            str(round(np.mean(gradient_pressure_rise_maximum), 2)) + '\t' + \
            str(round(np.mean(burning_rate), 2)) + '\t' + \
            str(round(np.mean(burning_duration), 2)) + '\t' + 'Gradient method' + '\n')
        global_counter = global_counter + 1
result_all.write('\n')
logging.info(u'\u2714\t\t\t\u263B Normal exit')
time_end = timeit.default_timer()
logging.info(u'\u2714\t\t\t\u231B Finished in ' + \
str(round(time_end - time_start,2)) + 's')

